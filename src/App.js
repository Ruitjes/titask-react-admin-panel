import React, { useState, useRef } from 'react';
import Routes from '../src/components/Routes';
import TopNavigation from './components/topNavigation';
import SideNavigation from './components/sideNavigation';
import Footer from './components/Footer';
import './index.css';
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBLink, MDBCol, MDBInput, MDBAlert } from 'mdbreact';
import  {useForm}  from "react-hook-form";
import axios from 'axios';

export default function App() {
  const { register, handleSubmit } = useForm();
  const ref = useRef(null);
  const [error, setError] = useState("");
  const [loginModalState, setLoginModalState] = useState();
  const [loggedIn, setLoggedIn] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false)

  function loginModalToggle() {
    setLoginModalState(!loginModalState);
  }

  function CheckIfAdmin(){
    var test;
    axios
    .get(process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + '/user/me': process.env.REACT_APP_PRO_MODE + '/user/me',{
        headers: {
            'Authorization': "Bearer " + localStorage.getItem("jwttoken"),
         }
        }).then((result) => {
          if(result.data.roles[0] == "ROLE_ADMIN"){
            setIsAdmin(true);
          }
        }).catch(error => {
            setLoggedIn(false);
            console.log(error)
        });

      return test;
  }

  async function onSubmit(data, e){
      e.target.reset();
      await axios({
              method: 'post',
              url: process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + '/user/signin?username='+ data.username +'&password=' + data.password: process.env.REACT_APP_PRO_MODE + '/user/signin?username='+ data.username +'&password=' + data.password,
              headers: {
                  'Content-Type': 'application/json',
              },
          }).then(response => {
            localStorage.setItem('jwttoken', response.data);
            CheckIfAdmin();
            if(isAdmin == true){
              localStorage.setItem('loggedIn', true);
              setLoggedIn(loggedIn);
            }else{
              setError("Je hebt geen admin rechten");
            }
          }).catch(error => {
            setError("Je gebruikersnaam en wachtwoord komen niet overeen op een account");
            console.log(error)
        });
    }

  if(localStorage.getItem('loggedIn')) {
    return (
      <div className="flexible-content">
        <TopNavigation />
        <SideNavigation />
        <main id="content" className="p-5">
          <Routes />
        </main>
        <Footer />
      </div>
    );
  } 
  else {
    return (
      <>
      <MDBContainer>          
      <MDBModal isOpen={true} toggle={loginModalToggle}>
        <MDBModalHeader className={"main-light-color"} toggle={loginModalToggle}>User</MDBModalHeader>
          <MDBModalBody className={"main-light-color"}>
            <MDBCol size="12">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <p className="h5 text-center mb-6 mt-3">Sign In</p>
                    {error ?  (
                      <>
                        <MDBAlert color="primary" >
                          {error}
                        </MDBAlert>
                      </>
                    ) : (
                      <>

                      </>
                    )}
                    <div className="">
                        <MDBInput style={{marginLeft: "45px"}} inputRef={register} aria-label={"Your name"} id={"username"} label="Your name" name="username" icon="user" group type="text" validate error="wrong"
                        success="right" />
                        <MDBInput style={{marginLeft: "45px"}} inputRef={register} aria-label={"Your password"} id={"password"} label="Your password" icon="lock" name="password" group type="password" validate />

                        <MDBBtn color="#5d5dff" name="button" type={"submit"} innerRef={register} className={"light-color"}>Login</MDBBtn>
                    </div>
                </form>
            </MDBCol>
          </MDBModalBody>
      </MDBModal>
    </MDBContainer>
      </>
    )

  }

  }

