import React from 'react';
import { Route, Switch } from 'react-router-dom';
import UsersPage from './pages/UsersPage';
import BoardsPage from './pages/BoardsPage';
import ColsPage from './pages/ColsPage';
import TasksPage from './pages/TasksPage';

class Routes extends React.Component {
  render() {
    return (
      <Switch>
        <Route path='/' exact component={UsersPage} />
        <Route path='/users' component={UsersPage} />
        <Route path='/boards' component={BoardsPage} />
        <Route path='/cols' component={ColsPage} />
        <Route path='/tasks' component={TasksPage} />
      </Switch>
    );
  }
}

export default Routes;
