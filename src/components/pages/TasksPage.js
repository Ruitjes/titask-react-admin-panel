import React, { useEffect, useState } from 'react';
import { MDBRow, MDBCol } from 'mdbreact';
// ES6
import ReactTable from 'react-table-v6'
import 'react-table-v6/react-table.css'
import axios from 'axios';

export default function TasksPage(){
  const [data, setData] = useState();
  
  const columns = [
  {
    Header: 'Id',
    accessor: 'id', // String-based value accessors!
    Cell: row => <div style={{ textAlign: "center" }}>{row.value}</div>
  }, 
  {
    Header: 'Board Name',
    accessor: 'boardName', // String-based value accessors!
    Cell: row => <div style={{ textAlign: "center" }}>{row.value}</div>
  }, 
  {
    Header: 'Column Status',
    accessor: 'status', // String-based value accessors!
    Cell: row => <div style={{ textAlign: "center" }}>{row.value}</div>
  }, 
  {
    Header: 'Title',
    accessor: 'title', // String-based value accessors!
    Cell: row => <div style={{ textAlign: "center" }}>{row.value}</div>
  }, 
  {
    Header: 'Content',
    accessor: 'content', // String-based value accessors!
    Cell: row => <div style={{ textAlign: "center" }}>{row.value}</div>
  }, 
  {
    Header: 'Delete',
    id: 'delete',
    accessor: (str) => 'delete',

    Cell: (tableProps) => (
      <div style={{ textAlign: "center" }}>
      <span style={{cursor:'pointer',color:'blue',textDecoration:'underline', textAlign: "center"}}
        onClick={() => {
          // ES6 Syntax use the rvalue if your data is an array.
          // const dataCopy = [...data];
          DeleteUser(tableProps.original.id);
          // It should not matter what you name tableProps. It made the most sense to me.
          // dataCopy.splice(tableProps.row.index, 1);
          // setData(dataCopy);
        }}>
      Delete
      </span>
      </div>
    ),
},
]

  function DeleteUser(id){
    axios
    .delete(process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + "/boards/delete/task/" + id: process.env.REACT_APP_PRO_MODE + "/boards/delete/task/" + id,{
      headers: {
              'Authorization': "Bearer " + localStorage.getItem("jwttoken"),
          }
      }).then((result) => {
          getAllUsers();
          // setData(result.data);
          // setItems(result.data)
      }).catch(error => {
          // window.location = "/";
      });
    }

  function getAllUsers(){
    axios
    .get(process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + "/boards/tasks": process.env.REACT_APP_PRO_MODE + "/boards/tasks",{
        headers: {
            'Authorization': "Bearer " + localStorage.getItem("jwttoken"),
        }
    }).then((result) => {
        setData(result.data);
        // setItems(result.data)
    }).catch(error => {
        // window.location = "/";
    });
  }

  useEffect(() =>{
    getAllUsers();
  }, [])

  return <ReactTable
    data={data}
    columns={columns}
  />
}

